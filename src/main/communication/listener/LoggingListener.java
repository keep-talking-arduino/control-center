package main.communication.listener;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class LoggingListener implements IBombListener, IMqttMessageListener {

    private Logger log = LoggerFactory.getLogger("LoggingListener");

    public LoggingListener(){
        log.info("Created.");
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage){
        log.debug("Received message. Topic=" + topic + " Message=" + new String(mqttMessage.getPayload()));

        switch (topic){
            case "log/debug":
                log.debug(new String(mqttMessage.getPayload()));
                break;
            case "log/info":
                log.info(new String(mqttMessage.getPayload()));
                break;
            case "log/warn":
                log.warn(new String(mqttMessage.getPayload()));
                break;
           case "log/error":
                log.error(new String(mqttMessage.getPayload()));
                break;
            default:
                log.error("Unknown logging received: " + new String(mqttMessage.getPayload()));
                break;
        }
    }
}
