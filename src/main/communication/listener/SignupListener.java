package main.communication.listener;

import org.eclipse.paho.client.mqttv3.IMqttMessageListener;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SignupListener implements IBombListener, IMqttMessageListener {

    private Logger log = LoggerFactory.getLogger("SignupListener");

    public SignupListener(){
        log.info("Created.");
    }

    @Override
    public void messageArrived(String topic, MqttMessage mqttMessage){
        log.debug("Received message. Topic=" + topic + " Message=" + new String(mqttMessage.getPayload()));
    }
}
