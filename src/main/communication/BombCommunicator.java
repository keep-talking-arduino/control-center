package main.communication;

import main.communication.listener.LoggingListener;
import main.communication.listener.SignupListener;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BombCommunicator {

    private static BombCommunicator instance;
    public static synchronized BombCommunicator getInstance(){
        if(instance == null){
            instance = new BombCommunicator();
        }
        return instance;
    }

    private BombCommunicator(){}

    Logger log = LoggerFactory.getLogger("BombCommunicator");

    String broker = "tcp://localhost:1883";
    String clientId = "JavaSample";


    MqttClient mqttClient;
    MqttConnectOptions connOpts = new MqttConnectOptions();
    MemoryPersistence persistence = new MemoryPersistence();

    SignupListener signupListener = new SignupListener();
    LoggingListener loggingListener = new LoggingListener();

    public void connect(){
        try {
            mqttClient = new MqttClient(broker, clientId, persistence);

            connOpts.setCleanSession(true);
            log.info("Connecting to broker: " + broker);
            mqttClient.connect(connOpts);

            addListeners();
        } catch (MqttException ex) {
            log.error(
                "Unable to connect to MQTT broker.\n" +
                "reason=" + ex.getReasonCode() + "\n" +
                "loc " + ex.getLocalizedMessage() + "\n" +
                "cause " + ex.getCause() + "\n",
                ex
            );
        }
    }

    private void addListeners() throws MqttException {
        mqttClient.subscribe("signup", signupListener);
        mqttClient.subscribe("log/#", loggingListener);
    }

    public void disconnect(){
        try {
            mqttClient.disconnect();
            log.info("Disconnected from MQTT Server");
        } catch (MqttException ex) {
            log.error("Unable to properly disconnect to MQTT broker.", ex);
        }
    }
}
