package main.bomb;

import lombok.Getter;
import main.bomb.module.Module;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

enum BombState {
    NOT_RUNNING,
    RUNNING,
    PAUSED,
    FINISHED
}

public class Bomb {

    Logger log = LoggerFactory.getLogger("bomb");

    // State
    private Timer timer = new Timer();

    @Getter
    private int strikes = 0;

    @Getter
    private BombState state = BombState.NOT_RUNNING;

    // Properties
    @Getter
    private int batteries = 0;

    @Getter
    private boolean frk = false;

    @Getter
    private boolean car = false;

    @Getter
    private boolean clr = false;

    @Getter
    private String serialNumber = "";

    private ArrayList<Module> modules = new ArrayList<>();

    public Bomb(){

    }

    public void start(){
        timer.setTime(0, 8);
        timer.start();

        timer.setOnComplete(this::explode);
        state = BombState.RUNNING;
    }

    public void explode(){
        log.info("Bomb exploded");
        state = BombState.FINISHED;
    }
}
