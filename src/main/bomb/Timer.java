package main.bomb;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.TimerTask;

public class Timer {
    private java.util.Timer timer = new java.util.Timer();
    private Logger log = LoggerFactory.getLogger("timer");

    private volatile int seconds;

    private long interval = 1000;

    private volatile boolean running = false;

    private TimerCallback onComplete;

    public interface TimerCallback{
        void run();
    }

    private void tick(){
        seconds--;
        log.debug("Timer: " + getTimeAsString());

        if(seconds <= 0){
            seconds = 0;
            timer.cancel();
            log.debug("Timer finished, countdown was cancelled.");
            if(onComplete != null){
                log.debug("Executing onComplete callback.");
                synchronized(onComplete) {
                    onComplete.run();
                }
            }
        }
    }

    public void setTime(int minutes, int seconds){
        setTime(60 * minutes + seconds);
    }

    public void setTime(int seconds){
        this.seconds = seconds;

        if(this.seconds > 5999){
            log.warn(getTimeAsString() + " is more than can be displayed in 4 digits. Limiting to 99:59");
            this.seconds = 5999;
        }

        log.debug("Time set to " + getTimeAsString());
    }

    public void start(){
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                tick();
            }
        }, interval, interval);

        running = true;
    }

    public void pause(){
        timer.cancel();

        running = false;
    }

    public void reset(){
        timer.cancel();
        seconds = 0;
        interval = 1000;

        running = false;
    }

    public void setInterval(long milliseconds){
        this.interval = milliseconds;

        if(running){
            pause();
            start();
        }
    }

    public void setOnComplete(TimerCallback onComplete) {
        this.onComplete = onComplete;
    }

    private int getMinutesFromSeconds(int seconds){
        return Math.floorDiv(seconds, 60);
    }

    private String getTimeAsString(){
        return String.format("%02d",getMinutesFromSeconds(seconds)) + ":" + String.format("%02d",seconds % 60);
    }

}


