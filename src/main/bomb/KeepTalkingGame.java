package main.bomb;

import main.communication.BombCommunicator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class KeepTalkingGame {

    Logger log = LoggerFactory.getLogger("KeepTalkingGame");

    private Bomb bomb = new Bomb();



    public KeepTalkingGame(){

    }

    public void run(){
        BombCommunicator.getInstance().connect();

        bomb.start();
    }

    public void exit(){
        BombCommunicator.getInstance().disconnect();
        System.exit(0);
    }
}
