package main.bomb.module;

import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;

public class Module {

    Logger log = LoggerFactory.getLogger("module");

    @Getter @Setter
    private String name = "";

    @Getter @Setter
    private int iterations = 0;

    private HashSet<Dependency> dependencies = new HashSet<>();

    public Module(){

    }

    public void addDependency(Dependency dependency){
        dependencies.add(dependency);
    }

    public boolean isSolved(){
        return iterations <= 0;
    }

}
